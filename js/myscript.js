$(document).ready(function () {
    $('.btnsearch').on('click', function (e) {
        $parent = $(this).parents('form');
        if ($parent.hasClass('opensearch')) {
            $parent.submit()
        } else {
            e.preventDefault();
            $parent.animate({}, 400, "swing", function () {
                $parent.addClass('opensearch');
                $parent.find('.txtsearch').focus()
            })
            //$parent.addClass('opensearch');
        }
    })
    $('.header_top-new').hover(function () {
    }, function () {
        $(this).find('form').removeClass('opensearch').animate({}, 400, "swing", function () {
        })
    })
    $('#cards-prev-link').hover(function () {
        var prev;
        var newclass;
        var title;
        var current = $('.main-nav ul.nav').find('li.current');
        if (current.prev().length > 0) {
            prev = current.prev();
            newclass = prev.attr('id');
            title = prev.find('a').text()
        } else {
            newclass = 'imaging';
            title = 'home';
        }
        $(this).find('.front-arrow-label')
            .addClass(newclass)
            .text("< " + title)
            .animate({width: 'toggle'}, 0);
    });
    $('#cards-next-link').hover(function () {
        var next;
        var newclass;
        var title;
        var current = $('.main-nav ul.nav').find('li.current');
        if (current.next().length > 0) {
            next = current.next();
            newclass = next.attr('id');
            title = next.find('a').text()
        } else {
            newclass = 'imaging';
            title = 'home';
        }

        $(this).find('.front-arrow-label')
            .addClass(newclass)
            .text(title + " >")
            .animate({width: 'toggle'}, 0);
    });

});

//light-box

$(document).ready(function () {
    $('li img').on('click', function () {
        var src = $(this).attr('src');
        var img = '<img src="' + src + '" class="img-responsive"/>';

        //start of new code new code
        var index = $(this).parent('li').index();
        var parentul = $(this).parent('li').parent('ul');
        var totalimg = parentul.children().length;
        var html = '';
        html += img;
        html += '<div style="display:block;">';
        html += '' +
        '<a class="right carousel-control controls next" href="' + (index + 2) + '">' +
        '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>' +
        '<span class="sr-only">Next</span>' +
        '</a>';
        html += '' +
        '<a class="left carousel-control controls previous" href="' + (index + 2) + '">' +
        '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>' +
        '<span class="sr-only">previous</span>' +
        '</a>';
        html += '</div>';

        html += '<div class="total-img">';
        html += index + 1 + "/" + totalimg;
        html += '</div>';

        $('#myModal').modal();
        $('#myModal').on('shown.bs.modal', function () {
            $('#myModal .modal-body').html(html);
            //new code
            $('a.controls').trigger('click');
        })
        $('#myModal').on('hidden.bs.modal', function () {
            $('#myModal .modal-body').html('');
        });

    });

    /////////////////
    if ($('.products .colR').innerHeight() > $('.products .colL .content').innerHeight()) {
        $('.products .colL .content').css({'height': $('.products .colR').innerHeight()});
    }
    if ($('.newsletters .movers-right-block').innerHeight() > $('.newsletters .movers-left-block .content').innerHeight()) {
        $('.newsletters .movers-left-block .content').css({'height': $('.newsletters .movers-right-block').innerHeight()});
    }
    if ($('.directories .wraplist ul li > a').innerHeight() > $('.directories .wraplist ul li div').innerHeight()) {
        $('.directories .wraplist ul li div').css({'height': $('.directories .wraplist ul li > a').innerHeight()});
    }
    $('.directories .wraplist ul li div').css({'padding-top': ($('.directories .wraplist ul li > a').innerHeight() - 34) / 2 + "px"});

})

$(window).resize(function () {
    if ($('.products .colR').innerHeight() > $('.products .colL .content').innerHeight()) {
        $('.products .colL .content').css({'height': $('.products .colR').innerHeight()});
    }
    if ($('.newsletters .movers-right-block').innerHeight() > $('.newsletters .movers-left-block .content').innerHeight()) {
        $('.newsletters .movers-left-block .content').css({'height': $('.newsletters .movers-right-block').innerHeight()});
    }
    if ($('.directories .wraplist ul li > a').innerHeight() > $('.directories .wraplist ul li div').innerHeight()) {
        $('.directories .wraplist ul li div').css({'height': $('.directories .wraplist ul li > a').innerHeight()});
    }
    $('.directories .wraplist ul li div').css({'padding-top': ($('.directories .wraplist ul li > a').innerHeight() - 34) / 2 + "px"});
});

//new code
$(document).on('click', 'a.controls', function () {
    var index = $(this).attr('href');
    $('.total-img').html(index + "/" + $('ul.row li').length);
    var src = $('ul.row li:nth-child(' + index + ') img').attr('src');
    $('.modal-body img').attr('src', src);
    var newPrevIndex = parseInt(index) - 1;
    var newNextIndex = parseInt(newPrevIndex) + 2;
    if ($(this).hasClass('previous')) {
        $(this).attr('href', newPrevIndex);
        $('a.next').attr('href', newNextIndex);
    } else {
        $(this).attr('href', newNextIndex);
        $('a.previous').attr('href', newPrevIndex);
    }
    var total = $('ul.row li').length + 1;
    //hide next button
    if (total === newNextIndex) {
        $('a.next').hide();
    } else {
        $('a.next').show()
    }
    //hide previous button
    if (newPrevIndex === 0) {
        $('a.previous').hide();
    } else {
        $('a.previous').show()
    }


    return false;
});


$(document).ready(function () {

    $('.li-category a').click(function(){
    var categorydropdown = $(this).next('ul.category-dropdown');
        if(categorydropdown.is(':hidden') == true) {
            categorydropdown.slideDown('normal');
        }
        else{
            categorydropdown.slideUp('normal');
        }

    });
});
